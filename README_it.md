<h1 align="center">Linux Cookbook</h1>

[English version](./README.md)

_di Filippo Gentile e Luca Pellegrini_

## Indice dei Contenuti

- [Introduzione](#introduzione)
  - [Sistemi operativi usati da noi](#sistemi-operativi-usati-da-noi)
- Al primo avvio
  - [Configurare un firewall](src/firewall_it.md)
  - Update, upgrade e riavvio
- [Gestore di pacchetti APT](src/apt_it.md)
  - Comandi di base
  - Gestore di pacchetti Synaptic
  - Modificare le APT sources
- [Bash: la Shell](src/bash_it.md)
  - File di configurazione di Bash
- [KDE Plasma Desktop](src/kde_it.md)
  - ...
  - [KDE Connect: integrazione tra desktop e smartphone](src/kde_it.md#kde-connect)
  - ...
- [Browser Web ed Estensioni](src/web_browsers_it.md)
  - Mozilla Firefox
  - Tor Browser
  - uBlockOrigin
  - uMatrix
  - CleanURLs
  - PrivacyRedirect
- [Client Email](src/email_clients_it.md)
  - Mozilla Thunderbird
  - Claws Mail
- [Crittografia OpenPGP](src/openpgp_it.md)
  - Verificare le firme OpenPGP con GnuPG
  - Come generare una coppia di chiavi OpenPGP
  - Usare la crittografia delle email in Thunderbird
  - Usare la crittografia delle email in Claws Mail
- [SSH](src/ssh_it.md)
  - Come generare una coppia di chiavi SSH
  - Configurazione del client OpenSSH
  - Configurazione del server OpenSSH
- [Systemd](src/systemd_it.md)
  - Alcuni comandi utili
- Gestire i file system
  - Il file /etc/fstab
  - Swap file e swap partition
    - Configurare l'ibernazione
- [Dual boot con Windows](src/dual_boot_it.md)
  - Disabilitare il "fast boot" in Windows e nelle impostazioni UEFI
  - Far leggere a Windows l'orologia di sistema come UTC
  - Auto-montare la partizione di Windows in Linux


* * *

## Introduzione

Questo _Linux Cookbook_

### Sistemi operativi usati da noi

#### Tuxedo OS 2

#### Debian 12 Bookworm

## Siti web e libri utili

* ArchWiki: [wiki.archlinux.org](https://wiki.archlinux.org/)
* alternativalinux: [www.alternativalinux.it](https://www.alternativalinux.it/)
* DigDeeper: [digdeeper.club](https://digdeeper.club/)

* William Shotts, _The Linux Command Line_, 2019 (disponibile online presso [LinuxCommand.org](https://linuxcommand.org/))

