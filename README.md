<h1 align="center">Linux Cookbook</h1>

[Versione in italiano](./README_it.md)

_by Filippo Gentile and Luca Pellegrini_

## Table of Contents

- [Introduction](#introduction)
  - [Operating Systems we use](#operating-systems-we-use)
- At first boot
  - [Setting up a firewall](src/firewall.md)
  - Update, upgrade and reboot
- [APT package manager](src/apt.md)
  - [Basic commands](src/apt.md#basic-apt-commands)
  - [Synaptic package manager](src/apt.md#synaptic-package-manager)
  - [Editing APT sources](src/apt.md#editing-apt-sources)
- Bash: the Shell
  - Bash configuration files
- [KDE Plasma Desktop](src/kde.md)
  - Tweaking some system settings
    - Grafics: compositor settings
    - Theming
    - Eyes protection
  - Dolphin: the file manager
    - Add buttons to the menu bar
    - Install KIO Admin plugin
  - Okular: the document viewer
    - Edit performance settings
  - KRunner: a very, very powerfull launcher
  - Activities and virtual desktops
  - [KDE Connect: integration between desktop and smartphone](src/kde.md#kde-connect)
  - Other suggested KDE apps
- [Web Browsers and Extensions](src/web_browsers.md)
  - Mozilla Firefox
  - Tor Browser
  - uBlockOrigin
  - uMatrix
  - CleanURLs
  - PrivacyRedirect
- [Email Clients](src/email_clients.md)
  - Mozilla Thunderbird
  - Claws Mail
- [OpenPGP encryption](src/openpgp.md)
  - Verifying OpenPGP signatures with GnuPG
  - How to generate an OpenPGP key pair
  - Using email encryption in Thunderbird
  - Using email encryption in Claws Mail
- [SSH](src/ssh.md)
  - How to generate an SSH key pair
  - OpenSSH client configuration
  - OpenSSH server configuration
- [The Linux Kernel](src/kernel.md)
  - [Kernel modules](src/kernel.md#kernel-modules)
- [Systemd](src/systemd.md)
  - A few useful commands
- Managing file systems
  - The /etc/fstab file
  - Swap file and swap partition
    - Setting up hibernation
- [Dual boot with Windows](src/dual_boot.md)
  - Disable "fast boot" in Windows and UEFI settings
  - Make Windows read hardware clock as UTC
  - Auto-mount Windows partition in Linux


* * *

## Introduction

This _Linux Cookbook_ is a small project we started with the goal of collecting in a unified place the knowledge base we acquired while using and experimenting with Linux and various related pieces of Free and Open Source software.
We made this Cookbook primarly for ourselves, to keep track of the applications we installed, the settings we tweaked, the extensions and plugins we started using, etc. etc.
Nonetheless, we hope that this knowledge will be useful for other people who have just started using Linux, or are willing to try it.

We are by no means "experts" or certified professionals in Linux system administration. We are just two friends who share a deep interest in Programming, Computer Science, the Free and Open Source Software ideals, Privacy-respecting software, and experimenting in general.


All content in this pages is provided "AS IS", without warranty of any kind.


### Operating Systems we use

#### Tuxedo OS 2

Made by [Tuxedo Computers](https://www.tuxedocomputers.com)

It is based on the latest Ubuntu LTS release (currently, 22.04), and use KDE Plasma as its Desktop Environment.
Although Tuxedo OS is based on a LTS release, it ships with latest version of all KDE applications, packaged by the Tuxedo team itself.

Filippo and Luca use Tuxedo OS on their laptop computers.

#### Debian 12 Bookworm

[Debian](https://www.debian.org/) is the upstream project which Ubuntu, and therefore all its derivatives, are based upon.

Luca uses Debian 12 Bookworm (the latest stable release), with KDE Plasma, on his laptop and desktop computers.


## Useful Websites and Books

* ArchWiki: [wiki.archlinux.org](https://wiki.archlinux.org/)
* alternativalinux: [www.alternativalinux.it](https://www.alternativalinux.it/) (Italian only)
* DigDeeper: [digdeeper.club](https://digdeeper.club/)

* William Shotts, _The Linux Command Line_, 2019 (available online at [LinuxCommand.org](https://linuxcommand.org/))

