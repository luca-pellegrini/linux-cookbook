# KDE Plasma Desktop

KDE is a worldwide community of volunteer software developers who work together to build and maintain Free and Open Sourse pieces of Software.

_Plasma_ is a desktop environment developed by the KDE community. A key feature of Plasma Desktop and of all KDE applications is that they are **very customizable**.

The _System Settings_ app allows you can customize your desktop experience extensively. Settings are divided in many pages (there are a lot of them).

## KIO Admin

Dolphin (KDE File manager) refuses to run as root.
To navigate and edit system folders you need KIO Admin
module which adds a context menu "Open with KIO Admin" and will ask you admin password.

```shell
sudo apt install kio-admin
```

## KDE Plasma theme

+ See [BUG 444043](https://bugs.kde.org/show_bug.cgi?id=444043)

## KDE Plasma taskbar

- Disable mouse wheel looping through activities
- Disable icon grouping

## KDE Web search

* Open KDE System Settings
* Go to Search
* Go to Plasma Search
* Go to keyword web search
* Click configure
* Disable all Big Data search engines
* Set DDG url with parameters as done in [Firefox section](./web_browsers.md#search-engine-helper)
<!--* Set DDG url with parameters as done in <a href="./web_browsers.md#ddg-url-parameters">Firefox section</a> !-->

## GTK+ Applications with KDE File Dialog

Add `export GTK_USE_PORTAL=1` to `.profile` and logout/login [Arch Wiki](https://wiki.archlinux.org/title/Uniform_look_for_Qt_and_GTK_applications#Consistent_file_dialog_under_KDE_Plasma)

## Eyes Protection

+ Enable RedShift
+ [SafeEyes](https://slgobinath.github.io/SafeEyes/)

## KDE Connect

Project's website: [kdeconnect.kde.org](https://kdeconnect.kde.org/)

_KDE Connect_ is an application that enables all your devices (smartphones, tablets and computers) to communicate with each other.

KDE Connect has a lot of features:
* Share files and link between devices
* Share the clipboard (what you copied on the phone can be instantly pasted on the computer)
* Sync phone notifications to your computers
* Control music and video playback on one device from another one
* Browse files on your phone from your computer
* And many more...

In order to use KDE Connect, you need to open the app on both your computer _and_ your phone; they must be connected to same network (e.g. your home Wi-Fi).
You should now be able to see in each screen the name of the device you wish to pair with.
If it's not working, you may need to [modify your firewall settings](./firewall.md#kde-connect).

You can find detailed information about KDE Connect on [this page](https://userbase.kde.org/KDEConnect) of the KDE UserBase Wiki.
