# Notes about the Linux Kernel

## Kernel modules

### How to list all loadable kernel modules?

Source: [StackExchange](https://unix.stackexchange.com/questions/184877/how-to-list-all-loadable-kernel-modules)

Question: _How do I list all the available modules in the system? In which directory are they located?_

By default `modprobe` loads modules from kernel subdirectories located in the `/lib/modules/$(uname -r)` directory. Usually all files have extension `.ko`, so you can list them with

```shell
find /lib/modules/$(uname -r) -type f -name '*.ko'
```

or, taking into account compressed files:

```shell
find /lib/modules/$(uname -r) -type f -name '*.ko*'
```

Each module can be also loaded by referring to its aliases, stored in the `/lib/modules/$(uname -r)/modules.alias` (and `modules.alias.bin`).

However, to load a modules successfully `modprobe` needs their dependencies listed in the file `/lib/modules/$(uname -r)/modules.dep` (and a corresponding binary version `modules.dep.bin`). If some module is present on the system, but is not on the list, then you should run a command `depmod` which will generate such dependencies and automatically include your module to modules.dep and `modules.dep.bin`.

Additionally, if the module is successfully loaded it will be listed in the file `/proc/modules` (also accessed via command `lsmod`).
