# APT package manager

APT (_Advanced Package Tool_) is the main command-line package manager for Debian, Ubuntu and their derivatives (including TUXEDO OS, Linux Mint, Pop!_OS, etc.).

It provides a set of command-line tools to download, install, remove, upgrade, configure and manage software packages on Debian and Ubuntu systems.

## Basic APT commands

Although grafical frontends like _Synaptic_ exist, it's always good to know the basic commands of the _apt_ command-line interface.

### Updating the repository index

```shell
sudo apt update
```

### Installing a package

```shell
sudo apt install <package>
```

### Reinstalling a package

```shell
sudo apt reinstall <package>
```

### Removing a package

```shell
sudo apt remove <package>
```

### Upgrading packages

```shell
sudo apt upgrade  # upgrades all packages
sudo apt upgrade <package>  # upgrades only a specific package
```

### Listing all installed packages

```shell
apt list --installed
```

### Searching for packages

```shell
apt search <string>  # searches in package names and descriptions
```

or:

```shell
apt list | grep <string>
```

## Synaptic package manager

_Synaptic_ is a graphical package management program for APT.

To install Synaptic:

```shell
sudo apt install synaptic
```

Note: on TUXEDO OS, Synaptic is preinstalled. You can find it in the applications menu, under the "Settings" category.

## Editing APT sources

APT downloads packages from one or more software repositories (sources). The specific repositories configured on your machine affect:
- What software packages are available for download
- What versions of packages are available
- Who packages the software

The main APT sources configuration file is at `/etc/apt/sources.list`.
To add custom sources, or edit the existing ones, you can edit this file (with root privileges) or create separate `*.list` files under the directory `/etc/apt/sources.list.d/`.

## Further reading

- Debian Wiki:
  - [Package Management](https://wiki.debian.org/PackageManagement)
  - [Package Management Tools](https://wiki.debian.org/PackageManagementTools)
  - [Apt CLI](https://wiki.debian.org/AptCLI)
  - [Synaptic](https://wiki.debian.org/Synaptic)
  - [Sources List](https://wiki.debian.org/SourcesList)
- Debian Reference Manual:
  - [Chapter 2. Debian package management](https://www.debian.org/doc/manuals/debian-reference/ch02.en.html)
- [apt manual page](https://manpages.debian.org/apt/apt.8)
- [sources.list manual page](https://manpages.debian.org/apt/sources.list.5)
