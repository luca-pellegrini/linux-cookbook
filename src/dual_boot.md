## Dual Boot Windows

- Disable Windows Fast Boot
- Make Windows read hardware clock as UTC (`REG_QWORD` for 64bit systems) [Arch Wiki](https://wiki.archlinux.org/title/System_time#UTC_in_Microsoft_Windows)
  ```
  reg add "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\TimeZoneInformation" /v RealTimeIsUniversal /d 1 /t REG_DWORD /f
  ```
- Disable Windows time adjustment (Run as admin) [Arch Wiki](https://wiki.archlinux.org/title/System_time#Multi-NTP_interaction)
  ```
  w32tm /unregister
  ```

