## Mail Client
1) Thunderbird
2) ClawsMail
    
    Install following plugins:
    * `claws-mail-fetchinfo-plugin` (Shows extra headers in received mails)
    * `claws-mail-litehtml-viewer` (Displays HTML mail content)
    * `claws-mail-perl-filter` (Mail search using Perl regular expression)
    * `claws-mail-pgpmime` (OpenPGP cryptography)

