## Web Browsers

Mozilla Firefox

### Extensions

#### uMatrix

[GitHub page](https://github.com/gorhill/uMatrix)

Blocks website requests with fine grained options

##### Settings


```
 https-strict: behind-the-scene false
 matrix-off: about-scheme true
 matrix-off: behind-the-scene true
 matrix-off: chrome-extension-scheme true
 matrix-off: chrome-scheme true
 matrix-off: moz-extension-scheme true
 matrix-off: opera-scheme true
 matrix-off: vivaldi-scheme true
 matrix-off: wyciwyg-scheme true
 noscript-spoof: * true
 referrer-spoof: * true
 referrer-spoof: behind-the-scene false
 * * * block
 * * css allow
 * * frame block
 * * image allow
 * 1st-party * allow
 * 1st-party frame allow
 * duckduckgo.com * block
 * improving.duckduckgo.com * block
 duckduckgo.com duckduckgo.com * inherit
 duckduckgo.com external-content.duckduckgo.com * block
 duckduckgo.com links.duckduckgo.com * block
 github.com collector.github.com * block
 github.com github.githubassets.com script allow
 howtogeek.com duckduckgo.com * block
 howtogeek.com improving.duckduckgo.com * block
 lunar.icu googlevideo.com media allow
 odysee.com cdn.cookielaw.org * block
 odysee.com cookielaw.org * block
 odysee.com gstatic.com * block
 odysee.com player.odycdn.com xhr allow
 odysee.com sentry.odysee.tv * block
 odysee.com www.gstatic.com * block
```

#### uBlockOrigin

Removes advertising

#### CleanURLs

[docs.cleanurls.xyz](docs.cleanurls.xyz)

Removes tracking parameters from URL before loading pages

#### PrivacyRedirect

- Invidious instance: [https://invidious.takebackourtech.org](https://invidious.takebackourtech.org)
- Always proxy videos

#### CanvasBlocker

[GitHub page](https://github.com/kkapsner/CanvasBlocker)

#### Search Engine Helper

[GitHub page](https://github.com/soufianesakhi/firefox-search-engines-helper)

- Add DuckDuckGo with disabled advertising
	+ [https://duckduckgo.com/?k1=-1&k5=2&kak=-1&kao=-1&kap=-1&kaq=-1&kau=-1&kax=-1&q=%s](https://duckduckgo.com/?k1=-1&k5=2&kak=-1&kao=-1&kap=-1&kaq=-1&kau=-1&kax=-1&q=%s)

#### Italian Dictionary

#### Disable Mozilla tracking in settings

+ See [debloat Firefox GitHub page](https://github.com/amq/firefox-debloat)
