# Setting up a firewall

## KDE Connect

KDE Connect uses dynamic ports in the range 1714-1764 for UDP and TCP. So, we need to make sure that these ports are open for both TCP and UDP.

#### ufw

If your firewall is **ufw**, you can open the necessary ports with:

```shell
sudo ufw allow 1714:1764/udp
sudo ufw allow 1714:1764/tcp
sudo ufw reload
```

#### firewalld

If your firewall is **firewalld**, you can open the necessary ports with:

```shell
sudo firewall-cmd --permanent --zone=public --add-service=kdeconnect
sudo firewall-cmd --reload
```

#### iptables

If your firewall is **iptables**, you can open the necessary ports with:

```shell
sudo iptables -I INPUT -i <yourinterface> -p udp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -I INPUT -i <yourinterface> -p tcp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT

sudo iptables -A OUTPUT -o <yourinterface> -p udp --sport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -o <yourinterface> -p tcp --sport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
```

Credits: [KDE UserBase Wiki](https://userbase.kde.org/KDEConnect#Troubleshooting)
