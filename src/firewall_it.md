# Configurare un firewall

## KDE Connect

KDE Connect usa porte dinamiche nel range 1714-1764 per UDP e TCP. Quindi, dobbiamo assicurarci che queste porte siano aperte sia per TCP e UDP.

#### ufw


Se il tuo firewall è **ufw**, puoi aprire le porte necessarie con:

```shell
sudo ufw allow 1714:1764/udp
sudo ufw allow 1714:1764/tcp
sudo ufw reload
```

#### firewalld

Se il tuo firewall è **firewalld**, puoi aprire le porte necessarie con:

```shell
sudo firewall-cmd --permanent --zone=public --add-service=kdeconnect
sudo firewall-cmd --reload
```

#### iptables

Se il tuo firewall è **iptables**, puoi aprire le porte necessarie con:

```shell
sudo iptables -I INPUT -i <yourinterface> -p udp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -I INPUT -i <yourinterface> -p tcp --dport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT

sudo iptables -A OUTPUT -o <yourinterface> -p udp --sport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -o <yourinterface> -p tcp --sport 1714:1764 -m state --state NEW,ESTABLISHED -j ACCEPT
```

Credits: [KDE UserBase Wiki](https://userbase.kde.org/KDEConnect#Troubleshooting)
