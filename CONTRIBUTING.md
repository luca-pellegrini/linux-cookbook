# How to contribute

The `README.md` file (and its translated version, e.g. `README_it.md`) has the following structure:
* A _Table of Contents_: lists all the topic covered in this Linux Cookbook (and those we plan to add in the near future), and each topic title should be a link to the corresponding page (or section inside a page) located in the `src` subdirectory.
* _Introduction_: describes the goal of this project, and lists the Operating Systems used by us
* _Useful Websites and Books_

All the contents of the Cookbook itself should be added into appropriate pages (i.e. `*.md` files) in the `src` subdirectory. 
Each page has a main topic (e.g. the Bash shell, the KDE desktop environment, web browsers and extensions, SSH, etc.), and it can be divided in many sections and subsections (2nd- and 3rd-level headers).
A link to each page and its sections and subsections should be placed in the _Table of Contents_.

Links should use the Markdown syntax:

**Link to another section in the same file as the current one** (a.k.a. _anchor link_):

```md
[Introduction](#introduction)
```

**Link to another file in a subdirectory**:

```md
[KDE Plasma Desktop](src/kde.md)
```

**Link to another file in the same (or parent) directory as the current one**:

```md
[Web Browsers](./web_browsers.md)  # same directory
[README](../README.md)  # parent directory
```

**Link to _a section_ in another file in the same (or parent) directory as the current one**:

```md
[Extensions](./web_browsers.md#extensions)  # same directory
[Introduction](../README.md#introduction)  # parent directory
```

**Link to an external website**:

```md
<https://www.example.com>  # or...
[Example](https://www.example.com)
```

## Referencing the sources

Wherever possible, we should reference the sources; for instance: main website or Git repo of the software application we are talking about, wiki articles we read, etc.
